import { combineReducers } from "redux";
import studentReducer from "../services/reducer";

const rootReducer = combineReducers({
    studentReducer: studentReducer,
});

export default rootReducer;