import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Route, Switch } from "react-router-dom";
import Navbar from "./components/navbar/navbar.component";
import Create from "./pages/Create";
import View from "./pages/View";

function App() {
  return (
    // <Router>
    <div className="container">
      <Navbar />
      <br />
      <Switch>
        <Route exact path="/" component={View} />
        <Route path="/create" component={Create} />
      </Switch>
    </div>
  );
}

export default App;
