const initialState = {
  studentAddStatus: false,
  studentGetStatus: false,
  studentUpdateStatus: false,
  studentData: [],
};

export default initialState;
