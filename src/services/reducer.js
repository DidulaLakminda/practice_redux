import ActionType from "./actionType";
import initialState from "./initialState";

const studentReducer = (state = initialState, action) => {
    switch(action.type){
        case ActionType.STUDENT_ADD_SUCCESS:
            return {
                ...state,
                studentAddStatus: true,
                studentGetStatus: false,
                studentUpdateStatus: false,
                studentData: action.payload,
            };
        
        case ActionType.STUDENT_ADD_FAILURE:
            return {
                ...state,
                studentAddStatus: false,
                studentGetStatus: false,
                studentUpdateStatus: false,
                studentData: action.payload,
            };

        case ActionType.STUDENT_GET_SUCCESS:
            return {
                ...state,
                studentAddStatus: false,
                studentGetStatus: true,
                studentUpdateStatus: false,
                studentData: action.payload,
            };

        case ActionType.STUDENT_GET_FAILURE:
            return {
                ...state,
                studentAddStatus: false,
                studentGetStatus: false,
                studentUpdateStatus: false,
                studentData: [],
            };

        case ActionType.UPDATE_STUDENT_SUCCESS:
            return {
                ...state,
                studentAddStatus: false,
                studentGetStatus: false,
                studentUpdateStatus: true,
                studentData: action.payload,
            };

        case ActionType.UPDATE_STUDENT_FAILURE:
            return {
                ...state,
                studentAddStatus: false,
                studentGetStatus: false,
                studentUpdateStatus: false,
                studentData: [],
            };    

        default:
            return state;
    }
}

export default studentReducer;