import axios from "axios";
import URL from "../config/config";
import ActionType from "./actionType";

export const AddStudent = (payload, history) => (dispatch) => {
  axios
    .post(URL.student.ADD, payload)
    .then((res) => {
      if (res.status === 200) {
        dispatch({
          type: ActionType.STUDENT_ADD_SUCCESS,
          payload: res.data,
        });
        history.push({ pathname: "/" });
      }
    })
    .catch((err) => {
      dispatch({
        type: ActionType.STUDENT_ADD_FAILURE,
        payload: err,
      });
    });
};

export const FetchStudent = (payload) => (dispatch) => {
  const config = {
    method: "GET",
    url: URL.student.GET,
    payload,
    headers: {
      "Content-Type": "application/json",
    },
  };

  axios(config)
    .then((res) => {
      if (res.status === 200) {
        dispatch({
          type: ActionType.STUDENT_GET_SUCCESS,
          payload: res.data,
        });
      }
    })
    .catch((err) => {
      dispatch({
        type: ActionType.STUDENT_GET_FAILURE,
        payload: err,
      });
    });
};

export const UpdateStudent = (payload, studentId, history) => (dispatch) => {
  axios
    .put(URL.student.UPDATE + "/" + studentId, payload)
    .then((res) => {
      if (res.status === 200) {
        dispatch({
          type: ActionType.UPDATE_STUDENT_SUCCESS,
          payload: res.data,
        });
        window.location.reload();
        history.push({ pathname: "/" });
      }
    })
    .catch((err) => {
      dispatch({
        type: ActionType.UPDATE_STUDENT_FAILURE,
        payload: err,
      });
      console.log(err);
    });
};

export const DeleteStudent = (studentId, history) => (dispatch) => {
  axios
    .delete(URL.student.DELETE + "/" + studentId)
    .then((res) => {
      if (res.status === 200) {
        dispatch({
          type: ActionType.DELETE_STUDENT_SUCCESS,
          // payload: res.data,
        });
        window.location.reload();
        history.push({ pathname: "/" });
      }
    })
    .catch((err) => {
      dispatch({
        type: ActionType.DELETE_STUDENT_FAILURE,
        // payload: err,
      });
      console.log(err);
    });
};
