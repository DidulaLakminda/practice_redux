import React, { Component } from "react";

export class StudentForm extends Component {
  render() {
    return (
      <div>
        <h3>{this.props.header}</h3>
        <form onSubmit={this.props.onSubmit}>
          <div className="form-group">
            <label>User Name: </label>
            <input
              type="text"
              required
              className="form-control"
              name="username"
              value={this.props.values.username}
              onChange={this.props.handleChange}
            />
          </div>
          <div className="form-group">
            <label>Email Address: </label>
            <input
              type="email"
              required
              className="form-control"
              name="email"
              value={this.props.values.email}
              onChange={this.props.handleChange}
            />
          </div>

          <div className="form-group mt-3">
            <input
              type="submit"
              value={this.props.buttonName}
              className="btn btn-primary"
            />
          </div>
        </form>
      </div>
    );
  }
}

export default StudentForm;
