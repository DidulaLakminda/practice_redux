import React, { Component } from "react";
import { DataGrid } from "@mui/x-data-grid";

export class StudentTable extends Component {
  render() {

    return (
      <div style={{ height: 400, width: "100%" }}>
        <DataGrid
          getRowId={(row) => row._id}
          rows={this.props.rows}
          columns={this.props.columns}
          pageSize={5}
          rowsPerPageOptions={[5]}
        //   checkboxSelection
        />
      </div>
    );
  }
}

export default StudentTable;
