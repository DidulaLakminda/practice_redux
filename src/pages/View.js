import React, { Component } from "react";
import { connect } from "react-redux";
import DeletePopup from "../components/popup/DeletePopup";
import StudentTable from "../components/studentTable/StudentTable";
import Edit from "../pages/Edit";
import { FetchStudent, DeleteStudent } from "../services/action";

class View extends Component {
  constructor() {
    super();
    this.state = {
      rowData: [],
      editData: [],
      editStatus: false,
      deleteId: "",
      open: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      rowData: nextProps.studentData,
    });
  }

  selectEditDetails = (data) => {
    this.setState({
      editStatus: true,
      editData: data,
    });
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  selectDeleteId = (id) => {
    this.setState({
      deleteId: id,
    });
    this.handleClickOpen();
    // this.props.DeleteStudent(id);
  };

  deleteSelectedStudent = () => {
    this.props.DeleteStudent(this.state.deleteId, this.props.history);
    this.handleClose();
  };

  componentDidMount() {
    this.props.FetchStudent();
  }

  render() {
    const columns = [
      { field: "_id", headerName: "ID", width: 300 },
      { field: "username", headerName: "UserName", width: 300 },
      { field: "email", headerName: "Email", width: 300 },
      {
        field: "action",
        headerName: "Action",
        width: 300,
        editable: true,
        renderCell: (params) => {
          return (
            <>
              <button
                className="btn btn-primary"
                onClick={() => this.selectEditDetails(params.row)}
              >
                Edit
              </button>
              <button
                className="btn btn-danger"
                onClick={() => this.selectDeleteId(params.row._id)}
              >
                Delete
              </button>
            </>
          );
        },
      },
    ];

    return (
      <>
        {!this.state.editStatus && (
          <>
            <StudentTable columns={columns} rows={this.state.rowData} />
            <DeletePopup
              handleClose={this.handleClose}
              open={this.state.open}
              deleteSelectedStudent={this.deleteSelectedStudent}
            />
          </>
        )}
        {this.state.editStatus && <Edit editData={this.state.editData} />}
      </>
    );
  }
}

const mapStateToProps = ({ studentReducer }) => {
  const { studentData } = studentReducer;
  return {
    studentData,
  };
};

const mapDispatchToProps = (dispatch) => ({
  FetchStudent: () => dispatch(FetchStudent()),
  DeleteStudent: (studentId, history) =>
    dispatch(DeleteStudent(studentId, history)),
});

export default connect(mapStateToProps, mapDispatchToProps)(View);
