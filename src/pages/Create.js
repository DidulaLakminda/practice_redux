import React, { Component } from "react";
import StudentForm from "../components/studentForm/StudentForm";
import { AddStudent } from "../services/action";
import { connect } from "react-redux";

class Create extends Component {
  constructor() {
    super();

    this.state = {
      username: "",
      email: "",
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  createStudent = (e) => {
    e.preventDefault();
    const payload = {
      username: this.state.username,
      email: this.state.email,
    };

    console.log(payload);

    this.props.AddStudent(payload, this.props.history);
  };

  render() {
    const { username, email } = this.state;
    return (
      <StudentForm
        header={"Create Student"}
        onSubmit={this.createStudent}
        handleChange={this.handleChange}
        values={{ username, email }}
        buttonName={"Create"}
      />
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  AddStudent: (payload, history) => dispatch(AddStudent(payload, history)),
});

export default connect(null, mapDispatchToProps)(Create);
