import React, { Component } from "react";
import StudentForm from "../components/studentForm/StudentForm";
import { UpdateStudent } from "../services/action";
import { connect } from "react-redux";

class Edit extends Component {
  constructor() {
    super();

    this.state = {
      username: "",
      email: "",
    };
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  componentDidMount() {
    this.setState({
      username: this.props.editData.username,
      email: this.props.editData.email,
    });
  }

  submitUpdateStudent = (e) => {
    e.preventDefault();
    const payload = {
      username: this.state.username,
      email: this.state.email,
    };

    this.props.UpdateStudent(
      payload,
      this.props.editData._id,
      this.props.history
    );
  };

  render() {
    const { username, email } = this.state;
    return (
      <StudentForm
        header={"Edit Student"}
        onSubmit={this.submitUpdateStudent}
        handleChange={this.handleChange}
        values={{ username, email }}
        buttonName={"Update"}
      />
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  UpdateStudent: (payload, studentId, history) =>
    dispatch(UpdateStudent(payload, studentId, history)),
});

export default connect(null, mapDispatchToProps)(Edit);
