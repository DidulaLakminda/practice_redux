const STUDENT_API = 'https://nioginpractice.herokuapp.com/users/';


const configUrl = {
    student: {
        ADD: `${STUDENT_API}add`,
        GET: `${STUDENT_API}getAll`,
        UPDATE: `${STUDENT_API}update`,
        DELETE: `${STUDENT_API}delete`,
    }
}

export default configUrl;